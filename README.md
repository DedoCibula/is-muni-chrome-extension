# IS MUNI Chrome Extension #

JavaScript Google Chrome extension created for Information System (IS) of Masaryk University.

Application features:

* Usage of Chrome API to query IS MUNI
* Asynchronous querying of IS mail box, noticeboard and forum
* JavaScript/JQuery, HTML5, CSS3