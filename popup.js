// constants
var url = 'https://is.muni.cz/auth/';
var bg = chrome.extension.getBackgroundPage();

// jQuery objects
var $icon = $('#icon');
var $sectionTitles = $('.section_title > span');
var $logged = $('.logged');
var $notLogged = $('#notLogged');
var $status = $('#status');
var $refreshing = $('#refreshing');
var $newMails = $('#newMailsNumber > a');
var $newNotices = $('#newNoticesNumber > a');
var $forum = $('#topics');
var $forumAnchors = null;
var $signIn = $notLogged.find('a').first();
var $refresh = $('#refresh');
var $toggle = $('#toggle');
var $close = $('#close');

// variables
var numberOfMails = $newMails.text();
var numberOfNotices = $newNotices.text();
var toggleButtonClass = $toggle.attr('class');
var refreshButtonClass = $refresh.attr('class');

// event handlers
$refresh.on('click', startRefreshingIS);
$toggle.on('click', toggleTimer);
$close.on('click', closeExtension);
$newMails.on('click', openMailBox);
$signIn.on('click', openSignIn);
$newNotices.on('click', openNoticeboard);
$icon.on('click', openIS);
$sectionTitles.on('click', toggleSectionBody);


function refreshIS() {
	chrome.extension.sendMessage({action: 'refreshIS'}, backgroundCallback);
}

function toggleTimer() {
	if (!refreshButtonClass)
		chrome.extension.sendMessage({action: toggleButtonClass}, backgroundCallback);
}

function setPreference(preference, value) {
	chrome.extension.sendMessage({action: preference, value: value});
}

function closeExtension() {
	window.close();
}

function updateTab(href) {
	chrome.tabs.query({ url: href }, function(tabs) {
		if (tabs.length !== 0) {
			chrome.tabs.update(tabs[0].id, { url: href, active: true });
		} else {
			chrome.tabs.create({ url: href });
		}
	});
}

function backgroundCallback(response) {
	var properties = typeof response === 'object' && Object.getOwnPropertyNames(response);
	var self = this;
	for (var property in properties) {
		self[properties[property]](response[properties[property]]);
	}
}

function invalidateMails(mails) {
    if (numberOfMails != mails[0]) {
        $newMails.text(mails[0] + " new mail" + ((mails[0] == 1) ? "" : "s"));  // temporary
        $newMails.attr('href', url + "mail/" + mails[1]);
        numberOfMails = mails;
    }
    console.log('mails refreshed');
}

function invalidateLogged(isLogged) {
	if (isLogged && $notLogged.is(':visible')) {
		$logged.show();
		$notLogged.hide();
		var preferences = bg.getPreferences();
		var mails = bg.getEmails();
		var notices = bg.getNotices();
		var topics = bg.getForumTopics();
		var timerRunning = bg.isTimerRunning();
		invalidateSectionBodies(preferences);
		invalidateMails(mails);
		invalidateNoticeboard(notices);
		invalidateForum(topics);
		invalidateToggleButton(timerRunning);
	} else if (!isLogged && $logged.is(':visible')) {
		$logged.hide();
		$notLogged.show();
	}
}

function invalidateNoticeboard(notices) {
	if (numberOfNotices != notices) {
		$newNotices.text(notices + " new notice" + ((notices == 1) ? "" : "s"));  // temporary
		numberOfNotices = notices;
	}
	console.log('notices refreshed');
}

function invalidateForum(topics) {
	if ($forumAnchors) {
		$.each($forumAnchors, function(index, anchor) {
			$(anchor).off('click');
		});
	}
	
	$forum.html(topics[1]);
	$forumAnchors = $forum.find('a');

	$.each($forumAnchors, function(index, anchor) {
		$(anchor).on('click', function(e) {
			e.preventDefault();
			openForumTopic(this);
		});
	});

	console.log('forum refreshed');
}

function invalidateToggleButton(timerRunning) {
	$toggle.toggleClass('stopTimer', timerRunning);
	$toggle.toggleClass('startTimer', !timerRunning);
	toggleButtonClass = $toggle.attr('class');
	$status.toggleClass('stopped', !timerRunning);
	$status.text((timerRunning ? 'Running' : 'Stopped'));
}

function invalidateRefreshButton(isRefreshing) {
	$refresh.toggleClass('spinning', isRefreshing);
	refreshButtonClass = $refresh.attr('class');
	$refreshing.toggleClass('hidden', !isRefreshing);
}

function invalidateSectionBodies(preferences) {
	$sectionTitles.each(function() {
		var sectionBody = $(this).parent().next();
		if (preferences[transformName(this.id)])
			sectionBody.hide();
		else
			sectionBody.show();
	});

	console.log('section bodies set from preferences');
}

function transformName(string) {
	return 'silence' + string.charAt(0).toUpperCase() + string.slice(1);
}

function openMailBox() {
	updateTab($newMails.attr('href'));
}

function openSignIn() {
	updateTab($signIn.attr('href'));
}

function openNoticeboard() {
	updateTab($newNotices.attr('href'));
}

function openForumTopic(anchor) {
	updateTab('https://is.muni.cz' + anchor.pathname + anchor.search);
}

function openIS() {
	updateTab(url);
}

function startRefreshingIS() {
	if (toggleButtonClass === 'stopTimer' && !refreshButtonClass) {
		invalidateRefreshButton(true);
		refreshIS();
	}
}

function toggleSectionBody() {
	var self = this;
	$(self).parent().next().slideToggle(400, function() {
		setPreference(transformName(self.id), $(this).is(':hidden'));
	});
}

if (bg.isLogged()) {
	invalidateLogged(true);
} else {
	invalidateLogged(false);
	refreshIS();
}