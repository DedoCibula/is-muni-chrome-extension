// constants
var ONE_SECOND = 1000;

// message variables
var numberOfEmails = 0;
var emailsSearch = "";
var numberOfNotices = 0;
var forumTopics = null;
var numberOfTopics = 0;
var logged = false;
var preferences = null;

// private variables
var callbacks = [ checkForum, checkMails, checkNoticeboard, checkImportantNotices ];
var events = {};
var notifications = {};
var response = {};
var audio = null;
var titleNews = false;
var notifyLogged = null;

// custom event handler
function Event(eventID) {
    var eventObj = eventID && events[eventID];

    if (!eventObj) {
        var callback = $.Callbacks('unique');
        eventObj = {
            publish: callback.fire,
            subscribe: callback.add,
            unsubscribe: callback.remove,
            unsubscribeAll: callback.empty,
            has: callback.has
        };
        events[eventID] = eventObj;
    }

    return eventObj;
}

function WaitAll(events, oneTimers) {
    var completed = [], result = {};
    var event = Event([].concat(events, oneTimers).join('_'));

    if (!event.completedCount) {
        var callback = function() {
            completed.push(id);
            if (completed.length == (event.completedCount || 
                events.length + oneTimers.length)) {
                event.publish(result);
                event.completedCount = events.length;
                completed = [];
                result = {};
            }
        };

        for (var id in events) {
            Event(events[id]).subscribe(function(data) {
                $.extend(result, data);
                callback();
            });
        }

        for (var id in oneTimers) {
            Event(oneTimers[id]).subscribe(function() {
                callback();
                Event(oneTimers[id]).unsubscribe(arguments.callee);
            });
        }
    }
    return event;
}

// wire up the listener
chrome.extension.onMessage.addListener(onMessage);

// subscribe notification click event
chrome.notifications.onClicked.addListener(function(id) {
    updateTab(notifications[id]);
});

function onMessage(message, sender, callback) {
    if (logged) {
        switch (message.action) {
            case 'refreshIS':
                response = {};
                BackgroundWorker.refresh();

                Event('responsePrepared').subscribe(function(object) {
                    callback(object);
                    Event('responsePrepared').unsubscribe(arguments.callee);
                });
                return true;
            case 'stopTimer':
                BackgroundWorker.stop();
                unsetCallbacks();
                response = {};
                callback({ 'invalidateToggleButton': false });
                return true;
            case 'startTimer':
                setupCallbacks();
                PreferenceManager.fetch();
                BackgroundWorker.init();
                BackgroundWorker.run();
                callback({ 'invalidateToggleButton': true });
                return true;
            case 'silenceMails':
            case 'silenceNoticeboard':
            case 'silenceForum':
                setPreferences(message.action, message.value);
                PreferenceManager.store(message.action, message.value);
                return true;
        }
    } else {
        var event = WaitAll(['responsePrepared'], ['preferencesChanged']);

        if (notifyLogged && event.has(notifyLogged)) {
            event.unsubscribe(notifyLogged);
            notifyLogged = null;
        }

        notifyLogged = function() {
            callback({ 'invalidateLogged': isLogged() });
            event.unsubscribe(notifyLogged);
        };

        event.subscribe(notifyLogged);
        return true;
    }
}

// setup
function setupCallbacks() {
    for (var callbackID in callbacks) {
        Event('isLogged').subscribe(callbacks[callbackID]);
    }

    Event('notLogged').subscribe(setUnlogged);

    Event('isLogged').subscribe(setLogged);

    Event('forumRefreshed').subscribe(createResponse);

    Event('mailBoxRefreshed').subscribe(createResponse);

    WaitAll(['mailBoxRefreshed'], ['preferencesChanged']).subscribe(fireMailNotification);

    Event('noticeboardRefreshed').subscribe(createResponse);

    WaitAll(['noticeboardRefreshed'], ['preferencesChanged']).subscribe(fireNoticeboardNotification);

    Event('preferencesFetched').subscribe(setPreferences);

    WaitAll(['responsePrepared'], ['preferencesChanged']).subscribe(changeBadge);
}

// reset
function unsetCallbacks() {
    for (var eventID in events) {
        events[eventID].unsubscribeAll();
    }
    events = {};
}

function createResponse(data) {
    $.extend(response, data);
    if (Object.keys(response).length == 3) {
        response['invalidateRefreshButton'] = false;
        Event('responsePrepared').publish(response);
        response = {};
    }
}

function playSound() {
    if (!audio)
        audio = new Audio('chime.ogg');
    audio.play();
}

var BackgroundWorker = {
    
    init: function() {
        var self = this;

        self.mainInterval = 60 * ONE_SECOND;
        self.url = "https://is.muni.cz/auth/";

        self.registerCallbacks();
    },

    registerCallbacks: function() {
        var self = this;

        Event('notLogged').subscribe(function() {
            if (!self.temporaryInterval) {
                self.temporaryInterval = 20 * ONE_SECOND;
                self.refresh();
            }
        });

        Event('isLogged').subscribe(function() {
            if (self.temporaryInterval) {
                self.temporaryInterval = null;
                self.refresh();
            }
        });
    },

    run: function() {
        var self = this;

        self.mainTimer = self.startExecuting(function() {
            self.pollIS()
                .done(function(results) {
                    self.checkLogin(results);
                })
                .fail(function(results) {
                    if (results.readyState == 4)
                        self.checkLogin(results.responseText);
                    else
                        Event('connectionError').publish();
                });

        }, self.temporaryInterval || self.mainInterval);
    },

    stop: function() {
        var self = this;

        clearInterval(self.mainTimer);
        self.mainTimer = null;
    },

    refresh: function() {
        var self = this;
        self.stop();
        self.run();
    },
    
    pollIS: function() {
        return $.ajax({
            url: this.url,
            dataType: "html"
        });
    },

    checkLogin: function(page) {
        var self = this;

        try {
            var title = $(page).filter('title').text();

            if (self.isLoginPage(title)) {
                Event('notLogged').publish();
            } else if (self.isMainPage(title)) {
                Event('isLogged').publish(page);
            }
        } catch (err) {
            // TODO
        }
    },

    isLoginPage: function(title) {
        return title == "Přihlášení do systému" || title == "System Log-In";
    },

    isMainPage: function(title) {
        return title == "Osobní administrativa" || title == "Personal Administration";
    },

    startExecuting: function(fn, interval) {
        fn();
        return setInterval(fn, interval);
    },

    isRunning: function() {
        return this.mainTimer !== null;
    }
};

var PreferenceManager = {
    
    init: function() {
        this.storeDelay = 2 * ONE_SECOND;

        this.dirtyFlag = false;
    },

    store: function(id, silenced, callback) {
        if (id) {
            var preference = {};
            preference[id] = silenced;
            this.innerStore(preference, callback);
        }
    },

    fetch: function() {
        var self = this;
        chrome.storage.sync.get(null, function(preferences) {
            if (!self.dirtyFlag)
                Event('preferencesFetched').publish(preferences);
        });
    },

    innerStore: function(preference, callback) {
        var self = this;

        self.dirtyFlag = true;
        self.clearPrevious(self.timeout);
        self.timeout = setTimeout(function() {
            self.timeout = null;
            chrome.storage.sync.set(preference, function() {
                if (typeof callback === 'function') 
                    callback();
                self.dirtyFlag = false;
            });
        }, self.storeDelay);
    },

    clearPrevious: function(timeout) {
        if (self.timeout) clearTimeout(self.timeout);
    }
};

// check mail - to be refactored
function checkMails(data) {
    try {
        var $mailBox = $(data).find('#port-posta');
        var pattern = /\d \w*/;
        var newMails = $mailBox.text().match(pattern);
        if (newMails) {
            numberOfEmails = Number(newMails[0][0]); // temporary
            emailsSearch = $mailBox.find('a').eq(1)[0].search; // temporary
        } else {
            numberOfEmails = 0;
            emailsSearch = "";
        }
        Event('mailBoxRefreshed').publish({ 'invalidateMails': getEmails() });
    } catch (err) {
        // TODO
        console.log(err);
        Event('mailBoxRefreshed').publish({ 'invalidateMails': getEmails() });
    }
}

// mail notification - to be refactored
function fireMailNotification(data) {
    var mails = data['invalidateMails']; // temporary
    if (notifications['mail'])
        chrome.notifications.clear('mail', function() { delete notifications['mail']; });
    if (preferences['silenceMails'] || mails[0] == 0) 
        return;
    var title = "New Mail Received";
    var message = "You've got " + mails[0] + " new mail" + ((mails[0] == 1) ? "." : "s.");
    var click = "https://is.muni.cz/auth/mail/" + mails[1];
    fireNotification(title, message, click, 'mail');
}

// check noticeboard - to be refactored
function checkNoticeboard(data) {
    try {
        var $noticeboard = $(data).find('#port-vyveska');
        var pattern = / \d\D/;
        var newNotices = $noticeboard.text().match(pattern);
        numberOfNotices = Number(newNotices[0][1]); // temporary
        Event('noticeboardRefreshed').publish({ 'invalidateNoticeboard': getNotices() });
    } catch (err) {
        // TODO
        console.log(err);
        Event('noticeboardRefreshed').publish({ 'invalidateNoticeboard': getNotices() });
    }
}

// noticeboard notification - to be refactored
function fireNoticeboardNotification(data) {
    var notices = data['invalidateNoticeboard']; // temporary
    if (notifications['noticeboard'])
        chrome.notifications.clear('noticeboard', function() { delete notifications['noticeboard']; });
    if (preferences['silenceNoticeboard'] || notices == 0) 
        return;
    var title = "New Notice Received";
    var message = "You've got " + notices + " new notice" + ((notices == 1) ? "." : "s.");
    var click = "https://is.muni.cz/auth/bb/sledovane/";
    fireNotification(title, message, click, 'noticeboard');
}

// check special notices - to be refactored
function checkImportantNotices(data) {
    try {
        var $specials = $(data).find('#t_vyveska_np');
        if ($specials.length != 0) {
            var anchor = $specials.find('a')[0];
            if (notifications['special'])
                chrome.notifications.clear('special', function() { delete notifications['special']; });
            var title = "You've got a special notice";
            var message = anchor.innerText;
            var click = "https://is.muni.cz" + anchor.pathname;
            fireNotification(title, message, click, 'special');
        }
    } catch (err) {
        // TODO
        console.log(err);
    }
}

// check forum - to be refactored
function checkForum(data) {
    try {
        $.ajax({
            type: 'POST',
            url: 'https://is.muni.cz/auth/rpanel_ajax.pl',
            data: { "akce": "sppo", "hodnota": "port-diskuse" },
            dataType: 'text',
        }).done(function(results) {
            forumTopics = results.split('#_#')[1];
            var newTopics = $(forumTopics).text().match(/\(\d+\)/);
            numberOfTopics = newTopics ? newTopics.length : 0;
            Event('forumRefreshed').publish({ 'invalidateForum': getForumTopics() });
        }).fail(function(results) {
            console.log(results);
            if (forumTopics) {
                Event('forumRefreshed').publish({ 'invalidateForum': getForumTopics() });
            }
        });
    } catch (err) {
        // TODO
        console.log(err);
    }
}

// badge - to be refactored
function changeBadge(data) {
    if (hasNews(data)) {
        chrome.browserAction.setBadgeText({ text: 'new!' });
        playSound();
        changeTitle(true);
    } else {
        chrome.browserAction.setBadgeText({ text: '' });
        changeTitle(false);
    }
}

function hasNews(data) {
    var news = $.grep(Object.keys(data), function(property) {
        var count = typeof data[property] === 'object' ? data[property][0] : data[property];
        return !preferences[property.replace('invalidate', 'silence')] 
            && !isNaN(count) && count != 0;
    });
    return news.length != 0;
}

function changeTitle(hasNews) {
    if (hasNews !== titleNews) {
        var titleMessage = hasNews ? 'New unread topics' : 'No unread topics';
        chrome.browserAction.setTitle({ title:  titleMessage });
        titleNews = hasNews;
    }
}

// notifications - to be refactored
function fireNotification(title, message, click, type) {
    chrome.notifications.create(type, {
        type: "basic",
        title: title,
        message: message,
        iconUrl: 'favicon2.png'
    }, function(id) {
        notifications[id] = click;
    });
}

function updateTab(href) {
    chrome.tabs.query({ url: href }, function(tabs) {
        if (tabs.length !== 0) {
            chrome.tabs.update(tabs[0].id, { url: href, active: true });
        } else {
            chrome.tabs.create({ url: href });
        }
    });
}

function getEmails() {
    return [ numberOfEmails, emailsSearch ];
}

function isLogged() {
    return logged;
}

function getNotices() {
    return numberOfNotices;
}

function getForumTopics() {
    return [ numberOfTopics, forumTopics ];
}

function getPreferences() {
    return preferences;
}

function isTimerRunning() {
    return BackgroundWorker.isRunning();
}

function setLogged() {
    logged = true;
}

function setUnlogged() {
    logged = false;
}

function setPreferences() {
    if (arguments.length == 1)
        preferences = arguments[0];
    else if (arguments.length == 2)
        preferences[arguments[0]] = arguments[1];
    Event('preferencesChanged').publish();
}

setupCallbacks();

// initialize PreferenceManager and fetch preferences
PreferenceManager.init();
PreferenceManager.fetch();

// start BackgroundWorker
BackgroundWorker.init();
BackgroundWorker.run();